# BioBuilder

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
### Creating a virtual env
```
mkdir ~/.venv
cd ~/.venv
python3 -m venv biobuilder
```
### Activating virtual env
```
source ~/.venv/biobuilder/bin/activate
```
### Install packages
```
pip install -r requirements.txt
```
### Pre-commit hooks
A framework for managing and maintaining multi-language pre-commit hooks - https://pre-commit.com/

* Black - Python code formatter
* iSort - Import sorter

1. Activate Virtual Environment
2. Verify pre-commit is installed `$ pre-commit --version`
3. Run `pre-commit install` to set up the git hook scripts
4. (optional) Run against all the files `pre-commit run --all-files`
5. (optional) Skip pre-commit hooks `git commit --no-verify`

## Standards
### Code
Follow the PEP8 Python Style Guide where applicable. Using Black & iSort will conform to the PEP8 style guide excluding line length of 79 characters.
Key points:
* lower_snake_case for variable names and functions
* CamelCase for classes
* Type hints for functions and parameters
### Git
#### Feature Branch Development
**Main**: Official release history

**Development**: Integration branch for features

**Feature**: Created from development branch. Merge requests are required to merge feature branches into development branch once feature has been completed. Feature branch naming conventions ‘feature/<ticket-id>-description’ e.g. feature/MCD-16-git-integration. Any hotfix or bugfix will include this as the prefix e.g. bugfix/<ticket-id>-description.
#### Commit Messages
**Subject**: Short and descriptive description - max 50 characters

**Body**: Separated with blank line from the subject. Explains what & why with each paragraph on a new line and capitalised.

    <Summarize change(s) in around 50 characters or less>
    
    <More detailed explanatory description of what and why in about 72 characters>

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
